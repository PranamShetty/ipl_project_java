package IPL_project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.*;

public class newMatch {

    public static void main(String[] args) {

        Map <String, Integer> teamMatchCount = new HashMap<>();

        String csvFile = "src/main/java/org/example/Data/matches.csv";

        try(BufferedReader br = new BufferedReader(new FileReader(csvFile))){
            String row;
            br.readLine();
                    while((row = br.readLine())!=null){
                        String []data = row.split(",");
                        String team = data[2];
                        int years = Integer.parseInt(data[1]);
//                        teamMatchCount.put(team, teamMatchCount.getOrDefault(team,0)+1);
                        if(teamMatchCount.containsKey(team)){
                            teamMatchCount.put(team,teamMatchCount.get(team)+1);
                        }else {
                            teamMatchCount.put(team,1);
                        }
                    }


                    for(Map.Entry<String,Integer>  entry: teamMatchCount.entrySet()){
                        System.out.println(entry.getKey() + "  :  " + entry.getValue());

                    }


        } catch (Exception e){
            e.printStackTrace();
        }






    }
}
