package IPL_project;

import java.io.*;
import java.util.*;

public class matchesPerYear {

    public static void main(String[] args) {
        String csvFile = "src/main/java/org/example/Data/matches.csv";

        Map<Integer,Integer> matchPerYear = new TreeMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                int year = Integer.parseInt(data[1]);
                matchPerYear.put(year, matchPerYear.getOrDefault(year,0)+1);
            }
            System.out.println(matchPerYear);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

