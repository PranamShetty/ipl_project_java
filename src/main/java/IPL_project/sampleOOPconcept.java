package IPL_project;

import org.w3c.dom.ls.LSOutput;

class StudentTotal {
    private String name;
    int marks;

    StudentTotal(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }

    StudentTotal() {
        name = "";
        marks = marks;
    }

    public String getName(){
        return name;
    }

    public int getMarks(){
        return marks;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMarks(int marks) {
        this.name = name;
    }

    public void displayInfo(int marks , String name){
        System.out.println("Student name " + name + "Marks " + marks);
    }

}

class highSchoolStudents extends StudentTotal{
    public void displayInfo(int marks , String name, int age ){
        System.out.println("Student name " + name + " Marks " + marks +" age " +age);
    }

}

class primaryStudents extends  StudentTotal{
    public void displayInfo(int marks , String name, int age , String favGame ){
        System.out.println("Student name " + name + " Marks " + marks + " age " +age + " Fav Game "+ favGame);
    }
}







public class sampleOOPconcept {
    public static void main(String [] args){
        StudentTotal student1 = new StudentTotal();
        student1.setName("aman");
        System.out.println(student1.getName());

        highSchoolStudents stud2 = new highSchoolStudents();
        stud2.displayInfo(25,"abhi",2);

        primaryStudents stud3 = new primaryStudents();
        stud3.displayInfo(25,"manoj",20,"football");

    }


}
