package IPL_project;
import java.io.*;
import java.util.*;

public class matchesWonByTeamPerYear {
    public static void main(String[] args) {
        String csvFile = "src/main/java/org/example/Data/matches.csv";
        Map<Integer, Map<String, Integer>> matchesWonByTeamPerYear = new TreeMap<>();


        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String row;
            br.readLine();
            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                int year = Integer.parseInt(data[1]);
                String wonByTeam = data[10];
                if (!matchesWonByTeamPerYear.containsKey(year)) {
                    matchesWonByTeamPerYear.put(year, new HashMap<>());
                }


                Map<String, Integer> teamWins = matchesWonByTeamPerYear.get(year);
                teamWins.put(wonByTeam, teamWins.getOrDefault(wonByTeam, 0) + 1);


            }

            System.out.println(matchesWonByTeamPerYear);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
