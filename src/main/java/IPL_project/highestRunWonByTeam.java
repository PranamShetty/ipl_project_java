package IPL_project;

import java.io.*;
import java.util.*;

public class highestRunWonByTeam {
    public static void main(String[] args) {
        String csvFile = "src/main/java/org/example/Data/matches.csv";
        Map<String, Integer> runsWonByTeam = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String row;
            br.readLine();

            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                String teamWon = data[10];
                int runs = Integer.parseInt(data[11]);

                if (runsWonByTeam.containsKey(teamWon)) {
                    int currentRuns = runsWonByTeam.get(teamWon);
                    if (runs > currentRuns) {
                        runsWonByTeam.put(teamWon, runs);
                    }
                } else {
                    runsWonByTeam.put(teamWon, runs);
                }
            }
            System.out.println(runsWonByTeam);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}


