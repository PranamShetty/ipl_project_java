package org.example;

class Vehicle {
    protected String brand;

    public void drive() {
        System.out.println("Driving the vehicle.");
    }
}

class Car extends Vehicle {
    private int numberOfDoors;

    public Car(String brand, int numberOfDoors) {
        this.brand = brand;
        this.numberOfDoors = numberOfDoors;
    }

    public void openDoors() {
        System.out.println("Opening " + numberOfDoors + " doors.");
    }
}

public class demoExample {
    public static void main(String[] args) {
        Car car = new Car("Toyota", 4);
        car.drive(); // Output: Driving the vehicle.
        car.openDoors(); // Output: Opening 4 doors.
    }
}
