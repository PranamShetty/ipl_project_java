class Student {
    private String name;
    private int marks;

    public Student(String name) {
        this.name = name;
    }

    public Student(int marks) {
        this.marks = marks;
    }

    public Student(String name, int marks) {
        this.name = name;
        this.marks = marks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public void print() {
        System.out.println("Name: " + name + ", Marks: " + marks);
    }
}

class HighSchoolStudent extends Student {
    private String gradeLevel;

    public HighSchoolStudent(String name, int marks, String gradeLevel) {
        super(name, marks);
        this.gradeLevel = gradeLevel;
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }
}

public class oopsConcept {
    public static void main(String[] args) {
        Student student1 = new Student("John");
        student1.setMarks(80);
        student1.print(); // Output: Name: John, Marks: 80

        HighSchoolStudent student2 = new HighSchoolStudent("Amanda", 95, "10th Grade");
        student2.print(); // Output: Name: Amanda, Marks: 95
        System.out.println(student2.getGradeLevel()); // Output: 10th Grade

        student2.setMarks(98);
        student2.print(); // Output: Name: Amanda, Marks: 98
    }
}
