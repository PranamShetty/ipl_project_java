package org.example;

import java.util.*;
import java.io.*;

public class extraRunsConcededByTeam {
    public static void main(String[] args) {
        String csvFileMatches = "src/main/java/org/example/Data/matches.csv";
        String csvFileDeliveries = "src/main/java/org/example/Data/deliveries.csv";

        List<Integer> year2016 = new ArrayList<>();
        Map<String, Integer> extraRunsByTeam = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFileMatches))) {
            String row;
            br.readLine();

            while ((row = br.readLine()) != null) {
                String[] data = row.split(",");
                int Year = Integer.parseInt(data[1]);
                int MatchID = Integer.parseInt(data[0]);
                if (Year == 2016) {
                    year2016.add(MatchID);
                }
            }

            try (BufferedReader brDeliveries = new BufferedReader(new FileReader(csvFileDeliveries))) {
                brDeliveries.readLine();

                while ((row = brDeliveries.readLine()) != null) {
                    String[] data = row.split(",");
                    int MatchID = Integer.parseInt(data[0]);
                    int extraConcede = Integer.parseInt(data[16]);
                    String BattingTeam = data[2];

                    if (year2016.contains(MatchID)) {
                        extraRunsByTeam.put(BattingTeam, extraRunsByTeam.getOrDefault(BattingTeam, 0) + extraConcede);
                    }
                }


                System.out.println("Extra runs conceded by each team in the year 2016:");
                for (Map.Entry<String, Integer> entry : extraRunsByTeam.entrySet()) {
                    System.out.println(entry.getKey() + ": " + entry.getValue());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
